import java.util.Scanner;

class dz33 {

    public static double pow(double x, int y) {
        double result = x;
        for (int i = 1; i < y; i++) {
            result *= x;
        }
        return result;
    }

    public static double integral(double a, double b, int n, int y) {
        double h = (b - a) / n;
        double result = 0;

        for (double x = a; x <= b; x = x + 2 * h) {
            double currentValue = pow(x - h, y) + 4 * pow(x, y) + pow(x + h, y);
            result = result + currentValue;
        }
        return result * (h / 3);
    }

    public static double integralRec(double a, double b, int n, int y) {
        double h = (b - a) / n;
        double result = 0;

        for (double x = a; x <= b; x = x + h) {
            double currentRectangle = pow(x, y) * h;
            result = result + currentRectangle;
        }

        return result;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double a = scanner.nextDouble();
        double b = scanner.nextDouble();


        int ns[] = {100, 1000, 10000, 100000, 1000000};
        int ys[] = {2, 3, 4, 5, 6, 7};

        for (int j = 0; j < ys.length; j++) {
            for (int i = 0; i < ns.length; i++) {
                double simpsonResult = integral(a, b, ns[i], ys[j]);
                double rectanglesResult = integralRec(a, b, ns[i], ys[j]);
                System.out.println("Rectangles - " + ns[i] + " - x^" + ys[j] + " = " + rectanglesResult);
                System.out.println("Simpson - " + ns[i] + " - x^" + ys[j] + " = " + simpsonResult);
            }
        }
    }
}