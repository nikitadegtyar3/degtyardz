import java.util.Scanner;

class dz {

	public static double f(double x) {
		return x * x;
	}

	public static double integral(double a, double b, double n) {
		double h = (b - a) / n;

		double result = 0;

		for (double x = a; x <= b; x = x + 2 * h) {
			double currentValue = f(x - h) + 4 * f(x) + f(x + h);
			result = result + currentValue;
		}
		return result * (h / 3);
	}
	public static double integralRec(double a, double b, double n) {
        double h = (b - a) / n;

        double result = 0;

        for (double x = a; x <= b; x = x + h) {
            double currentRectangle = f(x) * h;
            result = result + currentRectangle;
        }
        return result;
	}
		

	public static void mixIntegral(double a, double b, double n) {
        double res1 = integral(a, b, n);
        double res2 = integralRec(a, b, n);
        System.out.println("------------------------------------------------------------------");
        System.out.printf("%10s %20s %20s", "METHOD", "ITERATION", "RESULT");
        System.out.println();
        System.out.println("------------------------------------------------------------------");
        System.out.printf("%10s %20s %30s", "Rectangle", n, res2);
        System.out.println();
        System.out.printf("%10s %20s %31s", "Simpson", n, res1);
    }
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		double a = scanner.nextDouble();
		double b = scanner.nextDouble();
		int n = scanner.nextInt();

		
		mixIntegral(a, b, n);
	}
}