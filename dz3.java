import java.util.Random;
import java.util.Scanner;
class dz3 {

    public static Random random = new Random();

    public static int[] binarySearch() {
        int array[] = new int[10];
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(100);
        }
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println(array[array.length - 1]);

        int min, indexOfMin;

        for (int i = 0; i < array.length; i++) {
            min = array[i];
            indexOfMin = i;

            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < min) {
                    min = array[j];
                    indexOfMin = j;
                }
            }
            int temp = array[i];
            array[i] = array[indexOfMin];
            array[indexOfMin] = temp;
        }
        for (int i = 0; i < array.length - 1; i++) {
            System.out.print(array[i] + ", ");
        }
        System.out.println(array[array.length - 1]);
        return array;

    }

    public static void quickSearch(int[] array) {
        Scanner scanner = new Scanner(System.in);
        int numberForSearch = scanner.nextInt();

        int left = 0;
        int right = array.length - 1;
        int middle;

        boolean find = false;

        while (left <= right) {
            middle = (right + left) / 2;
            if (array[middle] < numberForSearch) {
                left = middle + 1;
            } else if (array[middle] > numberForSearch) {
                right = middle - 1;
            } else {
                System.out.println("Exists");
                find = true;
                break;
            }
        }
        if (find == false) {
            System.out.println("Not exists");
        }
    }

    public static void main(String[] args) {

        int arrayResult[] = binarySearch();
        quickSearch(arrayResult);


    }
}